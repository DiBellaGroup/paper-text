# encoding: UTF-8
require 'kramdown'
require 'erb'

orig_doc = File.read('Body.txt', encoding: 'UTF-8')

@latex = Kramdown::Document.new(orig_doc).to_latex
# Custom citations
@latex.gsub!(/\[@(.*?)\]/,'\cite{\1}')
# Custom figures
@latex.gsub!(/\\includegraphics{(.*[^}])\s(.*)}/,'\includegraphics[\2]{\1}')

# Use ERB to create latex file from template
template_file = File.open('latex_template.tex.erb', 'r').read
erb = ERB.new(template_file)
result = erb.result(binding)

File.write('paper.tex',result)

# Convert to PDF
def latex_it!
  `pdflatex paper.tex`
end

def bibtex_it!
  `bibtex paper`
end

latex_it!
bibtex_it!
latex_it!
latex_it! # yes twice, has to do with the way citations work

# Get rid of pdflatex excess output
%w(out aux log blg bbl fff lof).each do |ext|
  `rm paper.#{ext}`
end
