\documentclass{article}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{natbib}
\usepackage{amsmath}
\usepackage[nomarkers,figuresonly]{endfloat} %figures at end
\renewcommand{\bibnumfmt}[1]{#1}
\makeatletter
  \def\@eqnnum{{\normalfont \normalcolor [\theequation]}}
\makeatother
\begin{document}
\section{Introduction}\hypertarget{introduction}{}\label{introduction}

Iterative algorithms for image reconstruction are greatly improving MRI capabilities \cite{Lustig2007}\cite{block2007}. As applied to the problem of reconstructing non-Cartesian data, iterative reconstruction has found application to a wide variety of studies including cardiac perfusion \cite{shin}, angiography \cite{lee}, and quantitative oncological imaging \cite{smith}.

The general problem of converting non-Cartesian data into Cartesian data for MR image reconstruction is often called gridding \cite{gregerson2008}\cite{beatty2005}\cite{fessler2007}. Work in this field can be broadly divided into the reconstruction of fully-sampled data \cite{sha2003} and the reconstruction of undersampled data \cite{knopp2007}.

If given Cartesian data, iterative reconstruction techniques are able to use the highly optimized fast Fourier transform (FFT) at each iteration when converting between k-space and image space. Unfortunately, the FFT does not process non-Cartesian data and so a full, time-consuming, Fourier summation must be performed \cite{sarty1997}.

A popular fast algorithm for Fourier transforming non-Cartesian data is the non-uniform fast Fourier transform (NUFFT) provided by Fessler et. al. \cite{fessler}. This transform approximates a direct Fourier summation via Kaiser-Bessel interpolation. One NUFFT operation performs this interpolation followed by an FFT and is therefore slower than one operation of FFT alone. Fessler et. al. reported the interpolation step to require on average roughly twice as much time as the FFT step.

Some researchers, have instead converted their non-Cartesian MRI data into Cartesian data by interpolating onto nearby Cartesian grid points. For example McLeish et. al. \cite{McLeish2004}, used fast radial undersampled acquisitions to obtain free-breathing data of the heart. The first step in their workflow was to reconstruct individual interleaves with a three point linear interpolator prior to image registration. Likewise Adluru et. al \cite{adluru2009} interpolated radial data onto Cartesian grid points that were within 0.5 units of a measured sample. The approach for both is a \textbf{data-driven} interpolation which maps all acquired points to locations in Cartesian k-space. The remaining unmapped Cartesian locations are then unknown and often zeros or ones are left as their default values.

\begin{figure}
\begin{center}
\includegraphics[width=2in]{../figures/drawings/fig1-b.png}
\end{center}
\caption{The general approach in data-driven interpolation is to map (black arrows) non-Cartesian data (yellow circles) from non-Cartesian trajectories (red-orange lines) to Cartesian points (where black lines intersect). }

\end{figure}

In this paper, we compare three pre-reconstruction interpolators to the NUFFT approach in terms of speed and accuracy, treating the NUFFT approach as the gold-standard in this type of reconstruction problem.

\section{Theory and Methods}\hypertarget{theory-and-methods}{}\label{theory-and-methods}

For this study, we used the spatio-temporal constrained reconstruction (STCR) \cite{adluru2009} algorithm for image reconstruction. This method reconstructs the image by iteratively minimizing a cost function with tunable weight parameters $\alpha_1$ and $\alpha_2$:

\begin{displaymath}
C = \left|\left| WF m-d\right|\right|_2^2 + \alpha_1 \left|\left|\nabla_t m^2+\epsilon\right|\right|
+\alpha_2 \left|\left|\sqrt{\nabla_x m^2+\nabla_y m^2+\epsilon}\right|\right|
\end{displaymath}

This equation is described in detail in \cite{adluru2009}, so here we explain only the first term, which is the only term requiring a Fourier transform at each iteration. The first term is the data fidelity term. It is the $l_2$-norm squared of the difference between the estimated image $m$ and the measured k-space data $d$.

$F$ represents an operator capable of transforming the estimated image to its k-space representation and $W$ represents a binary mask of sampled locations. This mask is necessary because the FFT is a \textbf{grid-driven} method, which means it estimates an intensity for every point on the grid based on available data. Without the mask, the difference between $d$ and $Fm$ would be unnecessarily large at the unsampled points where $d$ is unknown but $Fm$ is not.

Note that for the NUFFT method, this mask is not necessary because it transforms a Cartesian image back to non-Cartesian k-space. That means at each execution of NUFFT a type of interpolation between non-Cartesian and Cartesian positions takes place. Therefore the fundamental difference between the NUFFT approach and the pre-interpolation approaches is that NUFFT performs an interpolation at each iteration, whereas the pre-interpolation strategy interpolates only one time.

We selected three interpolators to represent possible approaches to the problem of interpolating non-Cartesian data onto a Cartesian grid before iterative reconstruction. Here we will briefly describe the computational steps that all of the methods have in common and then the distinct methodologies of each one.

\subsubsection{Common Elements}\hypertarget{common-elements}{}\label{common-elements}

All four methods take as input a non-Cartesian k-space dataset and a corresponding trajectory which gives the k-space location of the acquired signal intensities. The three interpolators then map these acquired values to nearby Cartesian grid-points based on their algorithms.

\subsubsection{Nearest Neighbor Interpolation}\hypertarget{nearest-neighbor-interpolation}{}\label{nearest-neighbor-interpolation}

Nearest Neighbor (NN) is the most computationally simple of the three techniques. For each point in the acquired signal, the nearest Cartesian gridpoint is assigned that value. That means when multiple points map to the same location only the closest point to that location is used and other data is thrown-out. In our algorithm, a weighted average based on distance was used instead.

\begin{displaymath}
\frac
  {\sum_i^N S_i w_i }
  {\sum_i^N w_i}
\end{displaymath}

Where $S_i$ is the signal from non-Cartesian location $i$ and $w_i$ is the distance between non-Cartesian location $i$ and the new Cartesian location.  This way no samples go unused, but if more than one sample is within 0.5 units of a Cartesian location, then they are given weight proportional to their proximity to the desired point.

\subsubsection{3-Point Interpolation}\hypertarget{point-interpolation}{}\label{point-interpolation}

The 3-point interpolator we used is MATLAB's \cite{matlab} {\tt griddata} function and in this paper will be referred to as \textbf{Grid3}. The default interpolation method for {\tt griddata} is a triangulation-based linear interpolation. This algorithm fits a surface function to the datapoints at the sampled locations given in the trajectory. Then it uses that surface function to compute new values at the desired (Cartesian) points. These points were determined from the trajectory points provided with the dataset as was the binary mask used later in the STCR phase of reconstruction.

\subsubsection{GROG Interpolation}\hypertarget{grog-interpolation}{}\label{grog-interpolation}

The third method we investigated is a more sophisticated multi-coil method called GRAPPA Operator Gridding (GROG) \cite{seiberlich2007}. This method uses  GRAPPA operator theory \cite{griswold} to map data values from measured locations in k-space $(k_x, k_y)$ to unmeasured locations a distance $(\Delta k_x, \Delta k_y)$ away,

\begin{displaymath}
S(k_x + \Delta k_x, k_y + \Delta k_y) = G_x^{\Delta k_x} G_y^{\Delta k_y}S(k_x, k_y).
\end{displaymath}

In the above $G_x$ and $G_y$ are $N_c \times N_c$ GRAPPA coefficient matrices where $N_c$ is the number of coils. $G_y$ computes a unit shift in the positive $k_y$ direction and $G_x$ computes a unit shift in the positive $k_x$ direction. As explained in \cite{seiberlich2007}, a unit GRAPPA matrix with a kernel of size one (like $G_x$ and $G_y$) can be raised to a power to obtain a set of coefficients for a non-unit shift in k-space.

Though $G_x$ and $G_y$ can be obtained in a normal GRAPPA-like manner from a fully sampled part of k-space (such as auto-calibration signal lines), they can also be obtained directly from radial data as in the self-calibrating GROG (scGROG) method \cite{seiberlich2008}.

In this study we used scGROG to compute $G_x$ and $G_y$ and then shifted non-Cartesian values from their original locations to Cartesian locations using the formula given above.

\subsubsection{NUFFT Interpolation}\hypertarget{nufft-interpolation}{}\label{nufft-interpolation}

We compared all the above methods to the well-studied convolution-based interpolation employed by the NUFFT algorithm as the gold standard. This method takes as input the data and trajectory and then is able to transform non-Cartesian k-space into a Cartesian image space with minimal error. If the original data is undersampled, as in our case, the residual error is still quite large, and so the resultant image from NUFFT, as well as the other methods, is further iteratively reconstructed with STCR.

The software we used in this study was obtained from \cite{fesslercode} and then modified to fit the workflow we established for this study. We used the default parameters of a 6 x 6 kernel ($J$) and an oversampling factor ($K/N$) of 1.5. The simplest implementation of the other interpolators samples onto a unit grid, so it may seem inconsistent to use an oversampling ratio of 1.5 for NUFFT. In our experiments we found no difference between final reconstructions using a factor of 1.5 and 1, therefore we left NUFFT at its default value as our standard.

All the software used to produce these results is available online at \cite{breadcode}.

\subsection{Data}\hypertarget{data}{}\label{data}

We chose two datasets for comparative reconstruction. Both were acquired on a 3-T Verio MRI scanner (Siemens Medical Solutions, Erlangen, Germany) with a 32 channel phased-array receive coil and acquired using a saturation recovery sequence. The first dataset, which we will refer to as the \textbf{gated} dataset, consisted of 30 ray, golden-ratio radial, gated, contrast-enhanced cardiac perfusion data.  The scan parameters were as follows: slice thickness = 7 mm, spacing = 12.6 mm, 7 slices, flip angle = 10 degrees,  pixel size = 1.8 mm x 1.8 mm, TE = 1.37 ms, matrix size 144 x 30 rays. We used a saturation preparation pulse repeated every five slices. We chose slice five for comparative reconstruction, which had a saturation recovery time of approximately 302 ms.

The second, \textbf{ungated}, set was a 24 ray, golden-ratio, radial, ungated, contrast enhanced cardiac perfusion dataset. It was acquired with the following scan parameters: TR = 2.2 ms, TE = 1.25 ms, Matrix size 144 x 24 rays, slice thickness = 8 mm, spacing = 12.8  mm, 4 slices, flip angle = 10 degrees, pixel size = 1.9 mm x 1.9 mm. We chose slice three for comparative reconstruction which had a saturation recovery time of approximately 130 ms.

Before reconstruction, the ungated dataset was reconstructed using the Grid3 method and binned into near-systolic and near-diastolic time-frames using a self-gating method \cite{Likhite2015}. Only the k-space from the near-systolic frames were used for reconstruction.

Both datasets were first scaled so that the maximum was of order $10^3$ and then compressed in the coil dimension via principle component analysis to give the eight most representative virtual coils for full reconstruction.

We used one set of STCR weight parameters for the three non-convolution interpolators: $\alpha_1 = 0.03$, $\alpha_2 = 0.004$. A second set of weights was used when NUFFT was within the STCR iterations: $\alpha_1 = 0.1$, $\alpha_2 = 0.007$. This was due to scaling differences between the operations.

Lastly, we performed perfusion quantification analysis on the reconstructed images. The reconstructed images were registered using an Advanced Normalization Tools (ANTs)-based registration program as in \cite{Likhite2015}.  From there, image segmentation and processing was performed using MPI2D \cite{pack2009}. For segmentation, an expert user manually defined the boundaries of the epicardium and endocardium. To process a given dataset, the heart was divided into six azimuthal regions of interest. Then, using a two-compartment model, the perfusion was modeled from blood pool signal intensity and the measured arterial input function (AIF).

\subsubsection{NRMSE Analysis}\hypertarget{nrmse-analysis}{}\label{nrmse-analysis}

One measure of the performance of these methods is the normalized root-mean-square error (NRMSE). This was calculated in comparison to the NUFFT-based reconstruction with the following formula. $f$ is the NUFFT image volume, $g$ is the interpolation image volume, and the index $i$ runs over all of the pixels in the volume:

\begin{align}
\text{RMSE} &=
\sqrt{
  \frac{\sum_i^N (f_i - g_i)^2}
  {N}
} \\
\text{NRMSE} &= \frac{RMSE}{\bar{f}}.
\end{align}
Because of fundamental differences between reconstruction methods, the final images produces by each method are on slightly different intensity scales. To increase the relevancy of this kind of analysis, the images were scaled to minimize their RMSE in an ROI. That scale factor was calculated as shown in appendix A.

\subsubsection{Undersampling Simulation}\hypertarget{undersampling-simulation}{}\label{undersampling-simulation}

To further compare these methods, we simulated undersampling on the gated dataset. Because this radial dataset was collected with rays spaced by the golden angle \cite{winkelmann2007}, one can simply exclude rays from the end of the dataset when reconstructing and get a reasonable approximation of undersampling. From the original 30-ray gated dataset, we created a 29-ray, 28-ray, 27-ray etc. down to a 10-ray dataset and reconstructed each using the methods described above.

\section{Results}\hypertarget{results}{}\label{results}

\subsubsection{Reconstructed Images}\hypertarget{reconstructed-images}{}\label{reconstructed-images}

Figures 1 and 2 show the reconstructed images from each method. The most notable difference between methods is the intensity of the streaking-artifact outside of the body.

\begin{figure}
\begin{center}
\includegraphics[width=6in]{../figures/dicoms/gated.png}
\end{center}
\caption{Full reconstruction of the gated dataset. From left to right the methods used were NN, Grid3, GROG, and NUFFT.}

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=6in]{../figures/dicoms/ungated.png}
\end{center}
\caption{Full reconstruction of the ungated dataset. From left to right NN, Grid3, GROG, and NUFFT.}

\end{figure}

\subsubsection{Normalized Difference Images}\hypertarget{normalized-difference-images}{}\label{normalized-difference-images}

Figures 3 - 6 show the same images from figures 1 and 2 only they have been cropped more closely to the anatomical features of interest. The bottom rows of these figures show each method's difference when compared to NUFFT. The grayscale values were calculated as

\begin{displaymath}
\Delta = \frac{\left|f - g\right|}{\bar{f}}
\end{displaymath}

so we call them normalized difference images.

\begin{figure}
\begin{center}
\includegraphics[width=6in]{../figures/difference/24ray/24ray-fov-frame30.png}
\end{center}
\caption{Top row: reconstructed field-of-view image from ungated dataset. Bottom row, difference between method and NUFFT From left to right NN, Grid3, GROG, and NUFFT.}

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/24ray/24ray-roi-frame30.png}
\end{center}
\caption{Top row: reconstructed region-of-interest image from ungated dataset. Bottom row, difference between method and NUFFT From left to right NN, Grid3, GROG, and NUFFT.}

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=6in]{../figures/difference/30ray/30ray-fov-frame4.png}
\end{center}
\caption{Top row: reconstructed field-of-view image from gated dataset. Bottom row, difference between method and NUFFT From left to right NN, Grid3, GROG, and NUFFT.}

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/30ray/30ray-roi-frame4.png}
\end{center}
\caption{Top row: reconstructed region-of-interest image from gated dataset. Bottom row, difference between method and NUFFT From left to right NN, Grid3, GROG, and NUFFT.}

\end{figure}

\subsubsection{NRMSE Plots}\hypertarget{nrmse-plots}{}\label{nrmse-plots}

While the difference images help with a visual idea of the difference between these images, they are limited in showing only one time-frame.

Another measure of this comparison is the NRMSE of each time frame.

By experience, we learned that the intensities of the normalized difference images or NRMSE measure are strongly affected by the choice of field of view. This seems to be due to a significant brightness difference between methods when estimating certain areas of the images. In the ungated dataset, for example, the rib area, shown in figure 7, has the following mean intensities: NN = 1563, Grid3 = 1467, GROG = 1912, NUFFT = 1944.

\begin{figure}
\begin{center}
\includegraphics[width=6in]{../figures/difference/24ray/24ray-difference-detail.png}
\end{center}
\caption{Areas of the image with high signal intensity (white arrow) can upset the calculation of RMSE}

\end{figure}

To avoid allowing bright areas of the image to skew the results, the NRMSE difference images are cropped a little differently than the ones displayed in previous figures. An example of this is shown in figures 8 and 9.

\begin{figure}
\begin{center}
\includegraphics[width=4in]{../figures/difference/24ray/24ray_nrmse_crop.png}
\end{center}
\caption{Bright areas were cropped out (red box) of images before NRMSE analysis to avoid skewing the results.}

\end{figure}

Figures 10 - 13 show how the NRMSE across time frames varies.

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/24ray/fov-nrmse-plots.png}
\end{center}
\caption{Plot showing how NRMSE varies across time frames for the difference figures in figure 3 (the ungated dataset). The cropped region used here is not identical to the one in figure 3, it excludes the bright region in the lower left quadrant to avoid skewing the results. As shown in figure 8. }

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/24ray/roi-nrmse-plots.png}
\end{center}
\caption{The same as plot 9, with tighter cropping to demonstrate the effect of cropping on these measures. plot}

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/30ray/30ray-fov-nrmse.png}
\end{center}
\caption{Plot showing how NRMSE varies across time frames for the difference figures in figure 5 (the gated dataset). The cropped region used here is not identical to the one in figure 5, it excludes a bright region in the lower left quadrant to avoid skewing the results.}

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/30ray/30ray-roi-nrmse.png}
\end{center}
\caption{The same as plot 11, with tighter cropping to demonstrate the effect of cropping on these measures. plot}

\end{figure}

\subsubsection{Simulated Undersampling Plots}\hypertarget{simulated-undersampling-plots}{}\label{simulated-undersampling-plots}

In figures 14 and 15 we give the results of the simulated undersampling experiments. The horizontal axis shows the number of rays in the dataset and vertical axis shows the NRMSE between one time frame of that dataset and the same time frame from the fully-sampled NUFFT dataset.

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/30ray-reduction/fov-nrmse-30ray-reduction.png}
\end{center}
\caption{Simulated Undersampling of the gated gated dataset. These images were cropped to the same as figure 6. }

\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=5in]{../figures/difference/30ray-reduction/roi-nrmse-30ray-reduction.png}
\end{center}
\caption{Simulated Undersampling of the gated gated dataset. These images were cropped to the same as figure 7. }

\end{figure}

\subsubsection{Perfusion Quantification}\hypertarget{perfusion-quantification}{}\label{perfusion-quantification}

The perfusion quantification showed similar results for each method. The exception to this being the signal intensity curve from NN in the ungated dataset. Plots of mean myocardium signal intensity, averaged across the six ROIs, and arterial input function (AIF) are shown in figure 16.

\begin{figure}
\begin{center}
\includegraphics[width=5.5in]{../figures/perfusion/perfusion-plots.png}
\end{center}
\caption{Ungated and gated perfusion analysis results as calculated by the MPI2D software package.}

\end{figure}

\section{Discussion}\hypertarget{discussion}{}\label{discussion}

The primary reason to take the pre-interpolation approach is to save computation time by interpolating only once instead of at each iteration. In our trials we found that a forward NUFFT transform is about 2.3 times slower than FFT and an inverse transform is about 3.7 times slower. These two operations differ  because NUFFT is a grid-driven approach, calculating a value for every element of the grid. We therefore expect a measurable difference between the time it takes to perform a forward transform (which interpolates onto the smaller non-Cartesian k-space grid) and the inverse transform (which interpolates onto the larger Cartesian image grid).

The actual time advantage that one can expect when selecting one of these methods depends strongly on the details of the reconstruction method. Our implementation of STCR requires a forward and reverse transform on each iteration, but we do not see a 3-fold speed increase in total reconstruction time. This is because the Fourier transform step of the iterations account for about 6\% of the total computation required during an image's reconstruction leading to an overall reconstruction time difference of only about 12\% between an FFT-based reconstruction and an NUFFT-based one.

Among the three pre-reconstruction interpolation methods, the multi-coil method GROG produced results most like the NUFFT reconstructions. This is evidenced by the difference images, NRMSE calculations, and perfusion analysis which all showed a closer match between GROG and NUFFT than the other methods. Furthermore, in the ungated dataset, it is evident that the GROG method managed to significantly reduce streaking artifact outside the body where NUFFT failed to do so. This not only speaks to potential strengths of the GROG method but also the weakness of NUFFT as a gold-standard since it too is only an approximation.

The simulated undersampling trials demonstrated that all three methods perform similarly but again GROG performs better than the other methods. While NN and Grid3 are nearly identical, GROG keeps its RMS difference from NUFFT appreciably lower even at high acceleration factors.

It is interesting to note that in spite of some large visual differences between the final reconstructed images, when it comes to specific ROI analysis and perfusion quantification analysis, the streaking artifacts and low SNR have minimal impact. This is evidenced by the similarity of the signal intensity curves for both datasets.

The exception to this observation is the performance of NN in the perfusion analysis task on the ungated dataset. This result seems to have been caused by the quality of the data crossing a critical threshold where the registration software was unable to perform as well as it did with the other datasets.

\section{Conclusion}\hypertarget{conclusion}{}\label{conclusion}

Reconstructing undersampled non-Cartesian images presents a classic trade-off between quality and time. In this paper we compared common approaches to iterative reconstruction. The slower NUFFT method, when coupled with an iterative method, consistently produced excellent results. However, faster, FFT-based reconstruction can be performed at the cost of a pre-reconstruction interpolation step. When comparing the results of three interpolation methods we found that the multi-coil GROG approach consistently produced results most-similar to NUFFT.

Further work on this topic could focus on improving and analyzing the GROG method as a pre-reconstruction interpolator which is currently limited by a kernel size of 1 and is self-calibrating only on radial data. In the future we anticipate exploring the connection between multi-coil reconstruction methods like SENSE and GRAPPA when coupled to a multi-coil interpolation method like GROG.

\section{Appendix A - Calculation of NRMSE and scale factor}\hypertarget{appendix-a---calculation-of-nrmse-and-scale-factor}{}\label{appendix-a---calculation-of-nrmse-and-scale-factor}

First, we note that the sum-term inside the square-root is just the $l_2$-norm-squared difference between the images:

\begin{displaymath}
\left|\left|f-g\right|\right|_2^2
\end{displaymath}

The goal is to find a $\lambda$ which scales $g$ to minimize this difference. Because the values of this function are always positive and it is of second order, the minimum can be found by setting the first derivative with respect to $\lambda$ to zero.

\begin{align}
\frac{\partial}{\partial \lambda}
\left|\left|f-\lambda g\right|\right|_2^2 = 0 \\
2(-g)^T(f-\lambda g) = 0 \\
-g^Tf + \lambda g^Tg = 0 \\
\lambda g^Tg = g^Tf \\
\lambda = \frac{g^Tf}{g^Tg}.
\end{align}
\bibliography{Bibliography}{}
\bibliographystyle{unsrt}
\end{document}
